class AuthenticateController < ApplicationController
  
  def login
  	render :layout => 'login_register'
  end

  def sign_in
  	# find the user and authorize the user
  	if params[:email].present? && params[:password].present?
  		found_user = User.where(:email => params[:email]).first

  		if found_user 
  			authorized_user = found_user.authenticate(params[:password])
  		end
  	end

  	if authorized_user
  		# mark user as logged in 
  		initiate_session(authorized_user)

  		flash[:notice] = 'You are now logged in.'
  		redirect_to(controller: 'welcome', action: 'index')
  	else
  		flash[:notice] = "Invalid Email/Password combination!!!"
  		redirect_to(action: 'login')
  	end
  end

  def logout
  	# mark as logged out
  	destroy_session

  	flash[:notice] = "You are now logged out."
  	redirect_to(controller: 'welcome', action: 'index')
  end

  private 

  def initiate_session(authorized_user)
    session[:user_id] = authorized_user.id
    session[:user_email] = authorized_user.email
    session[:user_name] = authorized_user.name
    session[:user_role] = authorized_user.role.title
  end

  def destroy_session
    session[:user_id] = nil
    session[:user_email] = nil
    session[:user_name] = nil
    session[:user_role] = nil
  end

end
