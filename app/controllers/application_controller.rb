class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private 

  # user logged in as USER Role
  def logged_in_user
  	unless session[:user_id] && (session[:user_role] == Role::USER_ROLE)
  		
  		flash[:notice] = "Please Log in."
		redirect_to(:controller => 'authenticate', :action => 'login')

		return false
	else
		return true
  	end
  end

  # user logged in as MODERATOR Role
  def logged_in_moderator
  	unless session[:user_id] && (session[:user_role] == Role::MODERATOR_ROLE)
  		
  		flash[:notice] = "Please Log in."
		redirect_to(:controller => 'authenticate', :action => 'login')

		return false
	else
		return true
  	end
  end

  # user logged in as SUPER-ADMIN Rolel
  def logged_in_admin
  	unless session[:user_id] && (session[:user_role] == Role::SUPER_ADMIN_ROLE)
  		
  		flash[:notice] = "Please Log in."
		redirect_to(:controller => 'authenticate', :action => 'login')

		return false
	else
		return true
  	end
  end

end
