class UsersController < ApplicationController
  
  def register
  	@user = User.new;

  	render :layout => 'login_register'
  end

  def sign_up
    user_role = Role.where(:title => Role::USER_ROLE).first
    
  	@user = User.new(user_params)
    @user.role_id = user_role.id

    if @user.save

      flash[:notice] = 'Registration Sucessful, Please Login to continue'
      redirect_to controller: 'authenticate', action: 'login'
    else 
      render('register', :layout => 'login_register')
    end
  	
  end

  def show
  	# TODO
  end

  def edit
  	# TODO
  end

  private 

  def user_params
  	params.require(:user).permit(:first_name, :last_name, :email, :password)
  end
  
end