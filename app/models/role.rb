class Role < ActiveRecord::Base

	# captions
	SUPER_ADMIN_ROLE = 'Super Admin'
	MODERATOR_ROLE = 'Moderator'
	USER_ROLE = 'User'

	# ActiveRecord Relations
	has_many :users

	# named scopes

	# custom functions

end
