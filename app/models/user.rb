class User < ActiveRecord::Base

	# password encryption
	has_secure_password

	# active_record relationships
	belongs_to :role

	# validation rules
	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i

	validates :first_name, :presence => true, :length => {:minimum => 3, :maximum => 25}
	validates :last_name, :presence => true, :length => {:maximum => 50}
	validates :email, :presence => true, :length => {:maximum => 100}, 
			:format => EMAIL_REGEX, :uniqueness => true


	# named scopes
	scope :sorted, lambda {order("users.last_name ASC, users.first_name ASC")}

	# custom methods
	def name
		return "#{first_name} #{last_name}"
	end

end
