Rails.application.routes.draw do

  # root url
  root	'welcome#index'

  # authenticate routes
  get 	'auth/login'	=> 	'authenticate#login'
  post	'auth/sign_in'	=>	'authenticate#sign_in'
  get 	'auth/logout'	=>	'authenticate#logout'

  # user registration / signup
  get 	'register'	=>	'users#register'
  post 	'sign_up'	=>	'users#sign_up'

end