# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user_roles = Role.create([
	{title: 'Super Admin', caption: 'Super-Admin'},
	{title: 'Moderator', caption: 'Moderator'},
	{title: 'User', caption: 'User'},
])

admin_role = Role.where(:title => Role::SUPER_ADMIN_ROLE).first;

admin_user = User.create(
	role_id: admin_role.id, first_name: 'Admin', last_name: 'User', 
	email: 'admin@quizcms.com', password: 'adminadmin'
)