class AddRoleToUsers < ActiveRecord::Migration
  
  def change
  	add_reference :users, :role, index:true, after: :id
  end

end
