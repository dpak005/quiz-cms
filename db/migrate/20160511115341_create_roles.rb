class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :title, null: false
      t.string :caption, null: false

      t.timestamps null: false
    end
  end
end
