class CreateUsers < ActiveRecord::Migration
  
  def up
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name
      t.string :email, null: false, unique: true
      t.string :password_digest, null: false

      t.timestamps null: false
    end
  end

  def down 
  	drop_table :users;
  end

end
